package android.zinkp.crystalball;
import java.util.Random;
public class Predictions {

    private static Predictions predictions;
    private String[] answers;

    private Predictions () {
            answers= new String[] {
                    "Your wish has been granted!",
                    "Don't count on it.",
                    "Ask again later.",
                    "Maybe.",
                    "Yes.",
                    "No.",
                    "Maybe so.",
                    "Absolutely.",
                    "Definitely.",
                    "I can't answer that one.",
                    "I don't know.",
                    "I can't tell.",
                    "DENIED",
                    "GRANTED",
                    "I have no clue.",
                    "CLASSIFIED",
                    "Try again.",
                    "Too bad!",
                    "That's not gonna happen!",
                    "What do you think?",
                    "It all depends.",
                    "You'll never know!",
                    "Tough break!",
                    "Bad luck!",
                    "Talk to your mom about that!",
                    "?",
                    "I'm telling!",
                    "Yep.",
                    "Nope.",
                    "Ah, no.",
                    "Wrong!",
                    "Think again!",
                    "Your dreams will not come true!",
                    "Your dreams will come true!",
                    "Nah. The suspense is real."
            };
    }

    public static Predictions get() {
        if(predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }

    public String getPrediction() {
        int range = (answers.length -1);
        int rand = (int) (Math.random() * range) + 1;
        return answers[rand];
    }
}
